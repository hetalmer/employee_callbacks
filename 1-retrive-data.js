/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/
const fs = require("fs");
//1. retrive data for ids : [2,13,23]
//function to get the ids listed in array as argument
function retriveDataForIds(filename, idsList) {
	return new Promise(function (resolve, reject) {
		//read data form file asynchronously so return a promise
		fs.readFile(filename, "utf-8", function (err, data) {
			if (err) {
				reject(`error in reading data of specific Ids ${err}`);
			} else {
				//convert the data string to json
				data = JSON.parse(data);
				// filter all emplyees
				const result = data[Object.keys(data)].filter((values) => {
					//fetch id that is present in idsList
					if (idsList.includes(values.id)) {
						return values;
					}
				});
				resolve(result);
			}
		});
	});
}
//Group data based on companies.
//{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
function groupDataBasedCompany(filename) {
	return new Promise(function (resolve, reject) {
		fs.readFile(filename, "utf-8", function (err, data) {
			if (err) {
				reject(`error in reading data to goup by company ${err}`);
			} else {
				data = JSON.parse(data);
				const groupData = data[Object.keys(data)].reduce((result, values) => {
					// group by company so check result contains that company or not
					if (result.hasOwnProperty(values.company)) {
						result[values.company].push(values);
					} else {
						result = Object.assign({}, result, { [values.company]: [values] });
					}
					return result;
				}, {});
				resolve(groupData);
			}
		});
	});
}

//3. Get all data for company Powerpuff Brigade
//function to get the employee data by company which is passed as argumnet
function getAllDataByCompany(filename, companyName) {
	return new Promise(function (resolve, reject) {
		fs.readFile(filename, "utf-8", function (err, data) {
			if (err) {
				reject(`error in reading data to goup by company ${err}`);
			} else {
				data = JSON.parse(data);
				const filterByCompany = data[Object.keys(data)].filter((values) => {
					if (values.company === companyName) {
						return values;
					}
				});
				resolve(filterByCompany);
			}
		});
	});
}

//4. Remove entry with id 2.
//function to remove entry by id passed as argument
function removeById(filename, id) {
	return new Promise(function (resolve, reject) {
		fs.readFile(filename, "utf-8", function (err, data) {
			if (err) {
				reject(`Errpr in reading data by id ${err}`);
			} else {
				data = JSON.parse(data);
				const removeId = data[Object.keys(data)].filter((values) => {
					if (values.id !== id) {
						return values;
					}
				});
				resolve(removeId);
			}
		});
	});
}
//function to write data to json file which return a promise
function outputToJson(filename, data) {
	return new Promise(function (resolve, reject) {
		fs.writeFile(filename, JSON.stringify(data), function (err, data) {
			if (err) {
				reject(`Write operation not perform to json file ${err}`);
			} else {
				resolve(data);
			}
		});
	});
}
//function to print the error
function errorFunction(err) {
	console.log(err);
}
const promise = retriveDataForIds("data.json", [2, 13, 23]);
promise
	.then(function (data) {
		return outputToJson("output/1-output-retive-data.json", data);
	})
	.then(function (data) {
		return groupDataBasedCompany("data.json");
	})
	.then(function (data) {
		return outputToJson("output/2-output-group-by-company.json", data);
	})
	.then(function (data) {
		return getAllDataByCompany("data.json", "Powerpuff Brigade");
	})
	.then(function (data) {
		return outputToJson("output/3-output-filter-by-company.json", data);
	})
	.then(function (data) {
		return removeById("data.json", 2);
	})
	.then(function (data) {
		return outputToJson("output/4-output-remove-by-id.json", data);
	})
	.catch(function (err) {
		errorFunction(err);
	});
